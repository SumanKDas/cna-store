# Prerequisites #

Provides detailed instructions for installing the lab prerequisites

* [JDK 8](#jdk-8)
* [Maven](#maven)
* [Git](#git)
* [IDE for Java development](#ide-for-java-development)

**NOTE**: Install the latest version available that will work for your system. In most cases if you already have an older version of that particular software installed it should mostly work.

## JDK 8 ##

Install JDK 1.8 from http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Setup `JAVA_HOME` and include `JAVA_HOME/bin` in path.

For windows this can be done from the command prompt
```bat
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_171
set PATH=%PATH%;%JAVA_HOME%\bin
```

## Maven ##

Download Maven from here (https://maven.apache.org/download.cgi).

Unzip Maven binaries, set `MAVEN_HOME` and keep `MAVEN_HOME` in path.

For configuring proxy for maven refer https://maven.apache.org/guides/mini/guide-proxies.html

## Git ##

- The official build is available for download on the Git website. For a readily available version for windows, go to http://git-scm.com/download/win and the download will start automatically. 
- Note that this is a project called Git for Windows, which is separate from Git itself; for more information on it, go to https://git-for-windows.github.io/.

### Git Command Reference ###

In case you are not familiar with using Git, you can browse through this page (https://www.tutorialspoint.com/git/index.htm) and other similar resources to familirialize with how typicall you access a remote git repository and make changes in code and update the repository.
For our Bootcamp a minimal knowledge of accessing Git, checking out a repository branch is needed.

```sh
# Clone a repo
git clone <repo-url>

# Checkout to a specific branch
git checkout <branch>

# Stage and commit
git add <file>
git commit -m "Adding new file"

# Alternatively if all the files modified are already in the index 
git commit -am "commiting files that have already been added in a previous commit"

# Push to repository
git push origin <branch|master>
```

## IDE for Java development ##

There are several IDEs available for Java development. If you have one already installed use it or you can choose from one of the popular choices given below

  * [Spring Tool Suite](https://spring.io/tools)  
    Provides ready-to-use combinations of language support, framework support, and runtime support, and combine them with the existing Java, Web and Java EE tooling from Eclipse with support for technologies such as Pivotal Cloud Foundry, Gradle or Pivotal tcServer
  * [Eclipse IDE for JavaEE Developers](http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygen3a)
    Eclipse distribution tuned for creating Java EE and Web applications, including a Java IDE, tools for Java EE, JPA, JSF, Mylyn, EGit and others.
  * [IntelliJ IDEA](https://www.jetbrains.com/idea/download/)
    IDE with built-in support for git, JVM languages like Scala, Groovy, Kotlin; frameworks like Spring, JavaEE, Grails, Android and web development
